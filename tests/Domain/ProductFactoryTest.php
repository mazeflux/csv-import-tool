<?php

namespace App\Tests\Domain;

use App\Domain\ProductFactory;
use App\Entity\Product;
use PHPUnit\Framework\TestCase;

class ProductFactoryTest extends TestCase
{
    protected function setUp(): void
    {
    }

    /**
     * @dataProvider dataProvider
     */
    public function testTheDataIsNormalised($data, $dataValuesInPennies, $expected)
    {
        $factory = new ProductFactory();

        $product = $factory->createFromArray($data, $dataValuesInPennies);

        $this->assertEquals($expected['sku'], $product->getSku());
        $this->assertEquals($expected['description'], $product->getDescription());
        $this->assertEquals($expected['normal_price'], $product->getNormalPrice());
        $this->assertEquals($expected['special_price'], $product->getSpecialPrice());
        $this->assertNotNull($product->getCreatedAt());

    }

    public function dataProvider()
    {
        return [
            'values are converted to pennies' => [
                ['sku' => '8t7ih7', 'description' => 'desc', 'normal_price' => 700.50 ],
                false,
                ['sku' => '8t7ih7', 'description' => 'desc', 'normal_price' => 70050, 'special_price' => null],
            ],
            'empty special price is converted to null' => [
                ['sku' => '8t7ih7', 'description' => 'desc', 'normal_price' => 700.50, 'special_price' => '' ],
                false,
                ['sku' => '8t7ih7', 'description' => 'desc', 'normal_price' => 70050, 'special_price' => null],
            ],
            'missing special price is converted to null' => [
                ['sku' => '8t7ih7', 'description' => 'desc', 'normal_price' => 700.50, 'special_price' => '' ],
                false,
                ['sku' => '8t7ih7', 'description' => 'desc', 'normal_price' => 70050, 'special_price' => null],
            ],
            'penny values will remain the same' => [
                ['sku' => '8t7ih7', 'description' => 'desc', 'normal_price' => 70050 ],
                true,
                ['sku' => '8t7ih7', 'description' => 'desc', 'normal_price' => 70050, 'special_price' => null],
            ],
        ];
    }

}