<?php

namespace App\Tests\Repository;

use App\Entity\Product;
use App\Repository\ProductRepository;
use DateTimeImmutable;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class ProductRepositoryTest extends TestCase
{
    /** @var EntityManagerInterface|MockObject */
    protected $em;
    /** @var ManagerRegistry|MockObject */
    protected $registry;
    /** @var Product|MockObject */
    protected $product;

    protected function setUp(): void
    {
        $meta = $this->createMock(ClassMetadata::class);

        $this->em = $this->createMock(EntityManagerInterface::class);
        $this->em->method('getClassMetadata')->willReturn($meta);

        $this->registry = $this->createMock(ManagerRegistry::class);

        $this->product = new Product();
        $this->product->setSku('g67ub');
        $this->product->setDescription('desc');
        $this->product->setNormalPrice(1000);
        $this->product->setCreatedAt(new DateTimeImmutable);
    }

    public function testTheSaveMethodReturnsTheExistingProductWhenSkuExists()
    {
        $query = $this->createMock(AbstractQuery::class);
        $query->method('getOneOrNullResult')->willReturn($this->product);

        $builder = $this->createMock(QueryBuilder::class);
        $builder->method('select')->willReturnSelf();
        $builder->method('from')->willReturnSelf();
        $builder->method('andWhere')->willReturnSelf();
        $builder->method('setParameter')->willReturnSelf();
        $builder->method('getQuery')->willReturn($query);

        $this->registry->method('getManagerForClass')->willReturn($this->em);
        $this->em->method('createQueryBuilder')->willReturn($builder);

        $this->em->expects($this->never())->method('persist');

        $repo = new ProductRepository($this->registry);

        $updatedProduct = $repo->save($this->product);

        $this->assertNotNull($updatedProduct->getUpdatedAt());
    }

    public function testTheSaveMethodPersistsWhenNoExistingProductSku()
    {
        $query = $this->createMock(AbstractQuery::class);
        $query->method('getOneOrNullResult')->willReturn(null);

        $builder = $this->createMock(QueryBuilder::class);
        $builder->method('select')->willReturnSelf();
        $builder->method('from')->willReturnSelf();
        $builder->method('andWhere')->willReturnSelf();
        $builder->method('setParameter')->willReturnSelf();
        $builder->method('getQuery')->willReturn($query);

        $this->registry->method('getManagerForClass')->willReturn($this->em);
        $this->em->method('createQueryBuilder')->willReturn($builder);
        $this->em->expects($this->once())->method('persist');

        $repo = new ProductRepository($this->registry);

        $savedProduct = $repo->save($this->product);

        $this->assertNull($savedProduct->getUpdatedAt());
    }

    public function testTheFlushAndClearMethodWithClearAndFlushTheEntityManager()
    {
        $this->em->expects($this->once())->method('flush');
        $this->em->expects($this->once())->method('clear');

        $this->registry->method('getManagerForClass')->willReturn($this->em);

        $repo = new ProductRepository($this->registry);

        $repo->flushAndClear();
    }

    public function testTheDisableLoggingMethodSetTheSQLLoggerToNull()
    {
        $configuration = $this->createMock(Configuration::class);
        $configuration->expects($this->once())->method('setSQLLogger')->with(null);

        $conn = $this->createMock(Connection::class);
        $conn->expects($this->once())->method('getConfiguration')->willReturn($configuration);

        $this->em->expects($this->once())->method('getConnection')->willReturn($conn);

        $this->registry->method('getManagerForClass')->willReturn($this->em);

        $repo = new ProductRepository($this->registry);

        $repo->disableLogging();
    }
}
