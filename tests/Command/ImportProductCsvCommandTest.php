<?php

namespace App\Tests\Command;

use App\Command\ImportProductCsvCommand;
use App\Domain\ProductFactory;
use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Utils\CsvImporter\CsvImportTool;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ImportProductCsvCommandTest extends KernelTestCase
{
    /** @var MockObject|CsvImportTool */
    protected $csvTool;
    /** @var MockObject|ProductRepository */
    protected $repo;
    /** @var MockObject|Product */
    protected $product;
    /** @var MockObject|ValidatorInterface */
    protected $validator;
    /** @var MockObject|ProductFactory */
    protected $productFactory;

    protected function setUp(): void
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);
        $table = $em->getClassMetadata(Product::class)->getTableName();
        $em->getConnection()->executeQuery('TRUNCATE TABLE ' . $table);
    }

    protected function tearDown(): void
    {
        $em = self::$container->get(EntityManagerInterface::class);
        $table = $em->getClassMetadata(Product::class)->getTableName();
        $em->getConnection()->executeQuery('TRUNCATE TABLE ' . $table);

        parent::tearDown();
    }

    /**
     * @dataProvider scenarioDataProvider
     */
    public function testCommandScenarios($inputs, $expectedOutputCode, $expectedOutput)
    {
        $command = self::$container->get(ImportProductCsvCommand::class);

        $command = $this->executeCommand($command, $inputs);

        $this->assertEquals($expectedOutputCode, $command->getStatusCode());

        $this->assertRegExp($expectedOutput, $command->getDisplay());
    }

    public function scenarioDataProvider(): array
    {
        return [
            'Failure when file not found' => [
                ['filename' => 'aa'],
                Command::FAILURE,
                '/.*Unable to find file with name.*/'
            ],
            'Failure when no data in file' => [
                ['filename' => 'products_empty.csv', 'directory' => './tests/_data/Csv'],
                Command::FAILURE,
                '/.contains no data.*/'
            ],
            'Failure when invalid headers in csv' => [
                ['filename' => 'products_invalid_headers.csv', 'directory' => './tests/_data/Csv'],
                Command::FAILURE,
                '/.*Unexpected headers in the CSV file.*/'
            ],
            'Failure when invalid separator in csv' => [
                ['filename' => 'products_invalid_separator.csv', 'directory' => './tests/_data/Csv'],
                Command::FAILURE,
                '/.*Unexpected headers in the CSV file.*/'
            ],
            'Failure when no valid data imported' => [
                ['filename' => 'products_special_price_too_high.csv', 'directory' => './tests/_data/Csv'],
                Command::FAILURE,
                '/.*Total CSV rows 1. Rows with errors 1.*/'
            ],
            'Success when product created' => [
                ['filename' => 'products_valid.csv', 'directory' => './tests/_data/Csv'],
                Command::SUCCESS,
                '/.*Products created 1.*/'
            ],
            'Volume test with with 50k products' => [
                ['filename' => 'products_50k.csv', 'directory' => './tests/_data/Csv'],
                Command::SUCCESS,
                '/.*Total CSV rows 50000.*/'
            ],
            'Process should finish when malicious data given but no rows updated' => [
                ['filename' => 'products_malicious.csv', 'directory' => './tests/_data/Csv'],
                Command::FAILURE,
                '/.*Total CSV rows 3. Rows with errors 3.*/'
            ],
        ];
    }

    /**
     * This helper method abstracts the boilerplate code needed to test the
     * execution of a command.
     *
     * @param array $arguments All the arguments passed when executing the command
     */
    private function executeCommand(ImportProductCsvCommand $command, array $arguments): CommandTester
    {
        // this uses a special testing container that allows you to fetch private services
        $command->setApplication(new Application(self::$kernel));

        $commandTester = new CommandTester($command);
        $commandTester->execute($arguments);

        return $commandTester;
    }
}
