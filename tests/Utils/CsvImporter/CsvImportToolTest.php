<?php

namespace App\Tests\Utils\CsvImporter;

use App\Utils\CsvImporter\CsvImportTool;
use App\Utils\CsvImporter\Exception\FileNotFoundException;
use App\Utils\CsvImporter\Exception\InvalidCsvFileFormatException;
use App\Utils\CsvImporter\Exception\NoDataInFileException;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Optional;
use Symfony\Component\Validator\Constraints\Required;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CsvImportToolTest extends KernelTestCase
{
    /** @var MockObject|ValidatorInterface */
    protected $validator;
    /** @var string */
    protected $dir;

    protected function setUp(): void
    {
        $this->validator = $this->createMock(ValidatorInterface::class);
        $this->dir = './tests/_data/Csv';
    }

    public function testWhenValidFileFoundThenReturnResults()
    {
        $tool = new CsvImportTool($this->validator);
        $data = $tool->importFromFileName($this->dir, 'products_valid.csv');

        $this->assertIsArray($data);
    }

    public function testWhenFileDoesNotExistThenThrowAnException()
    {
        $this->expectException(FileNotFoundException::class);

        $tool = new CsvImportTool($this->validator);
        $tool->importFromFileName($this->dir, 'a');
    }

    public function testWhenFileDoesNotContainDataThenThrowAnException()
    {
        $this->expectException(NoDataInFileException::class);

        $tool = new CsvImportTool($this->validator);
        $tool->importFromFileName($this->dir, 'products_empty.csv');
    }

    public function testWhenValidationFailsThenThrowAnException()
    {
        self::bootKernel();

        $this->expectException(InvalidCsvFileFormatException::class);

        $validator = self::$container->get(ValidatorInterface::class);

        $tool = new CsvImportTool($validator);

        $tool->importFromFileName($this->dir, 'products_invalid_headers.csv', new Collection([
            'sku' => new Required(),
            'description' => new Required(),
            'normal_price' => new Required(),
            'special_price' => new Optional(),
        ]));

    }
}
