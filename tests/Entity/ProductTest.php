<?php

namespace App\Tests\Entity;

use App\Entity\Product;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProductTest extends KernelTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
    }

    /**
     * @dataProvider productDataProvider
     */
    public function testValidationFailsWhenIncorrectDataInProduct($product, $valid)
    {
        $validation = self::$container->get(ValidatorInterface::class);

        $errors = $validation->validate($product);

        $this->assertEquals($valid, (count($errors) === 0));

    }

    public function productDataProvider(): array
    {
        return [
            'empty sku' => [
                (new Product())
                    ->setSku('')
                    ->setDescription('asd')
                    ->setNormalPrice(1000)
                    ->setSpecialPrice(100)
                    ->setCreatedAt(new DateTimeImmutable()),
                false
            ],
            'empty description' => [
                (new Product())
                    ->setSku('asdasd123')
                    ->setDescription('')
                    ->setNormalPrice(1000)
                    ->setSpecialPrice(100)
                    ->setCreatedAt(new DateTimeImmutable()),
                false
            ],
            'negative normal price' => [
                (new Product())
                    ->setSku('asdasd123')
                    ->setDescription('asdq3d')
                    ->setNormalPrice(-100)
                    ->setSpecialPrice(100)
                    ->setCreatedAt(new DateTimeImmutable()),
                false
            ],
            'negative special price' => [
                (new Product())
                    ->setSku('asdasd123')
                    ->setDescription('asdq3d')
                    ->setNormalPrice(1000)
                    ->setSpecialPrice(-100)
                    ->setCreatedAt(new DateTimeImmutable()),
                false
            ],
            'special price more than normal price' => [
                (new Product())
                    ->setSku('asdasd123')
                    ->setDescription('asdq3d')
                    ->setNormalPrice(1000)
                    ->setSpecialPrice(2000)
                    ->setCreatedAt(new DateTimeImmutable()),
                false
            ],
            'no created at' => [
                (new Product())
                    ->setSku('asdasd123')
                    ->setDescription('asdq3d')
                    ->setNormalPrice(1000)
                    ->setSpecialPrice(2000),
                false
            ],
            'valid product' => [
                (new Product())
                    ->setSku('asdasd123')
                    ->setDescription('asdq3d')
                    ->setNormalPrice(1000)
                    ->setSpecialPrice(600)
                    ->setCreatedAt(new DateTimeImmutable()),
                true
            ],
            'invalid product' => [
                (new Product()),
                false
            ],
            'normal price value too high' => [
                (new Product())
                    ->setSku('asdasd123')
                    ->setDescription('asdq3d')
                    ->setNormalPrice(9999999999)
                    ->setCreatedAt(new DateTimeImmutable()),
                false
            ],
            'description contains invalid data' => [
                (new Product())
                    ->setSku('asdasd123')
                    ->setDescription("<IMG SRC='javascript:alert(\"RSnake says, 'XSS'\")'>")
                    ->setNormalPrice(999)
                    ->setCreatedAt(new DateTimeImmutable()),
                false
            ],
            'sku contains invalid data' => [
                (new Product())
                    ->setSku("<IMG SRC='javascript:alert(\"RSnake says, 'XSS'\")'>")
                    ->setDescription("desc")
                    ->setNormalPrice(9999999999)
                    ->setCreatedAt(new DateTimeImmutable()),
                false
            ],
            'special price equal to the normal price' => [
                (new Product())
                    ->setSku('asdasd123')
                    ->setDescription('asdq3d')
                    ->setNormalPrice(1000)
                    ->setSpecialPrice(1000)
                    ->setCreatedAt(new DateTimeImmutable()),
                false
            ],
        ];
    }
}