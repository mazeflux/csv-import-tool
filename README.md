#Docker setup

### Mac

The easiest way is to download and install the docker desktop.
https://docs.docker.com/docker-for-mac/install/

### Alternative/Other OS

Alternative methods are available if not running on Mac or would prefer not to use the docker desktop.
https://docs.docker.com/compose/install/

###Docker Compose

Once downloaded, run `docker-compose up` from inside the root directory to create the docker container.

This will take a few minutes to create the container due to the composer install.

# MySql

The database and Product table is created when the docker container is created.

Below are the details to access the DB.

`Host: 127.0.0.1`

`Username: admin`

`Password: 12dlql*41`

`Port: 3308`

`database: test_db`

#Commands

From the root directory, run:

`docker exec import_tool php bin/console app:import-product-csv {filename}`

Options:
- Add the `-v` option to run in verbose mode

#CSV

The import command will look for filenames in the `./data/ProductCsv` directory.

A sample CSV has been added to the `./data/ProductCsv/Sample` directory.

There are further test csv's in the `./tests/_data/Csv` directory.

#Tests

Run `docker exec import_tool php bin/phpunit` from the root directory.

The tests may take a minute or so to complete due to a volume test within the unit tests. The initial test run will take
longer because the composer test files are installed.