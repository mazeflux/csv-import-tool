#!/bin/bash

/usr/local/bin/wait-for-it.sh database:3306 -t 30

composer install
php bin/console doctrine:migrations:migrate --no-interaction --allow-no-migration
tail -f /dev/null