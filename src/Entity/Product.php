<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $sku;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     *
     */
    private $normal_price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $special_price;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $UpdatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @Assert\NotBlank(message="The sku cannot be empty")
     * @Assert\Regex(pattern="/^[a-z\-0-9]+$/", message="The sku can only contain alphanumeric characters")
     */
    public function getSku(): ?string
    {
        return $this->sku;
    }

    public function setSku(string $sku): self
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * @Assert\NotBlank(message="The description cannot be empty")
     * @Assert\Regex(pattern="/^[a-z\-0-9]+$/", message="The description can only contain alphanumeric characters")
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @Assert\PositiveOrZero(message="The normal_price must be a positive number")
     * @Assert\Length(
     *      max = 9,
     *      maxMessage = "The normal_price can not be greater than 999,999,999 pennies"
     * )
     */
    public function getNormalPrice(): ?int
    {
        return $this->normal_price;
    }

    /**
     * @param int $normal_price The monetary value in pennies
     * @return $this
     */
    public function setNormalPrice(int $normal_price): self
    {
        $this->normal_price = $normal_price;

        return $this;
    }

    /**
     * @Assert\PositiveOrZero(message="The special_price must be a positive number")
     * @Assert\Length(
     *      max = 9,
     *      maxMessage = "The special_price can not be greater than 999,999,999 pennies"
     * )
     */
    public function getSpecialPrice(): ?int
    {
        return $this->special_price;
    }

    /**
     * @param int|null $special_price The monetary value in pennies
     * @return $this
     */
    public function setSpecialPrice(?int $special_price): self
    {
        $this->special_price = $special_price;

        return $this;
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {
        if ($this->getSpecialPrice() >= $this->getNormalPrice())
        {
            $context->buildViolation('special_price should be less than normal price')
                ->atPath('special_price')
                ->addViolation();
        }
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->UpdatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $UpdatedAt): self
    {
        $this->UpdatedAt = $UpdatedAt;

        return $this;
    }
}
