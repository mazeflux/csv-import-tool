<?php

namespace App\Domain;

use App\Entity\Product;
use DateTimeImmutable;

class ProductFactory
{
    public function createFromArray(array $data, $dataValuesInPennies = false): Product
    {
        // Make sure an undefined/empty 'special_price' is resolved to a null value.
        $data['special_price'] = is_numeric($data['special_price'] ?? null) ? $data['special_price'] : null;

        if (false === $dataValuesInPennies) {
            // Convert the prices into the penny value for storage.
            if ($data['special_price'] !== null) {
                $data['special_price'] *= 100;
            }

            $data['normal_price'] *= 100;
        }

        $product = new Product();
        $product->setSku($data['sku']);
        $product->setDescription($data['description']);
        $product->setNormalPrice($data['normal_price']);
        $product->setSpecialPrice($data['special_price']);
        $product->setCreatedAt(new DateTimeImmutable());

        return $product;
    }
}
