<?php

namespace App\Repository;

use App\Entity\Product;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use LogicException;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends EntityRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(ManagerRegistry $registry)
    {
        $this->entityManager = $registry->getManagerForClass(Product::Class);

        if ($this->entityManager === null) {
            throw new LogicException(sprintf(
                'Could not find the entity manager for class "%s".',
                self::Class
            ));
        }
        parent::__construct($this->entityManager, $this->entityManager->getClassMetadata(Product::Class));
    }

    public function findOneBySku(string $value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.sku = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function save(Product $product): Product
    {
        $existingProduct = $this->findOneBySku($product->getSku());

        if (null === $existingProduct) {
            $this->entityManager->persist($product);

            return $product;
        }

        $existingProduct->setDescription($product->getDescription());
        $existingProduct->setNormalPrice($product->getNormalPrice());
        $existingProduct->setSpecialPrice($product->getSpecialPrice());
        $existingProduct->setUpdatedAt(new DateTimeImmutable);

        return $existingProduct;
    }

    public function flushAndClear(): void
    {
        $this->entityManager->flush();
        $this->entityManager->clear();
    }

    public function disableLogging(): void
    {
        $this->entityManager->getConnection()->getConfiguration()->setSQLLogger(null);
    }
}
