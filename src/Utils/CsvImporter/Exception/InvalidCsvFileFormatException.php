<?php

namespace App\Utils\CsvImporter\Exception;

class InvalidCsvFileFormatException extends \RuntimeException
{
    /** @var string */
    protected $errors;
    public function __construct(string $fileName, string $csvSeparator, string $errors)
    {
        $this->errors = $errors;

        parent::__construct(sprintf(
            'Unexpected headers in the CSV file %s. Is "%s" the correct separator for this file?',
            $fileName,
            $csvSeparator
        ));
    }

    public function getErrors(): string
    {
        return $this->errors;
    }
}
