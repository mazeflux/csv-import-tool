<?php

namespace App\Utils\CsvImporter\Exception;

class FileNotFoundException extends \RuntimeException
{
    public function __construct(string $fileName, string $dir)
    {
        parent::__construct(sprintf(
            'Unable to find file with name "%s" in the directory "%s"',
            $fileName,
            $dir
        ));
    }
}