<?php

namespace App\Utils\CsvImporter\Exception;

class NoDataInFileException extends \RuntimeException
{
    public function __construct(string $fileName)
    {
        parent::__construct(sprintf('File "%s" contains no data', $fileName));
    }
}