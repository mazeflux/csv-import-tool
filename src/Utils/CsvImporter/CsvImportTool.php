<?php

namespace App\Utils\CsvImporter;

use App\Utils\CsvImporter\Exception\FileNotFoundException;
use App\Utils\CsvImporter\Exception\InvalidCsvFileFormatException;
use App\Utils\CsvImporter\Exception\NoDataInFileException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CsvImportTool
{
    /** @var ValidatorInterface */
    protected $validator;
    /** @var string */
    protected $csvSeparator = '|';

    /**
     * CsvImportTool constructor.
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }


    /**
     * @param string $dir
     * @param string $fileName
     * @param Constraint|null $headerValidation Optionally validate the header.
     * @return mixed[] The array of csv roles mapped to the column name.
     */
    public function importFromFileName(string $dir, string $fileName, Constraint $headerValidation = null): array
    {
        $filePath = $dir .'/' . $fileName;

        if (false === file_exists($filePath)) {
            throw new FileNotFoundException($fileName, $dir);
        }

        $csv = array_map(function ($rows) {
            return str_getcsv($rows, $this->getCsvSeparator());
        }, file($filePath,FILE_SKIP_EMPTY_LINES));

        $columns = array_shift($csv);

        $importedRows = [];
        foreach ($csv as $i => $row) {
            $importedRows[$i] = array_combine($columns, $row);
        }

        if (count($importedRows) === 0) {
            throw new NoDataInFileException($fileName);
        }

        if (null !== $headerValidation) {
            $error = $this->validator->validate($importedRows[0], $headerValidation);
            if (count($error) > 0) {
                throw new InvalidCsvFileFormatException($fileName, $this->getCsvSeparator(), (string) $error);
            }
        }

        return $importedRows;
    }


    public function getCsvSeparator(): string
    {
        return $this->csvSeparator;
    }

    public function setCsvSeparator(string $csvSeparator): self
    {
        $this->csvSeparator = $csvSeparator;

        return $this;
    }
}
