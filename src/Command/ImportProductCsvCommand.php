<?php

namespace App\Command;

use App\Domain\ProductFactory;
use App\Repository\ProductRepository;
use App\Utils\CsvImporter\CsvImportTool;
use App\Utils\CsvImporter\Exception\FileNotFoundException;
use App\Utils\CsvImporter\Exception\InvalidCsvFileFormatException;
use App\Utils\CsvImporter\Exception\NoDataInFileException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Optional;
use Symfony\Component\Validator\Constraints\Required;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * A console command that imports a product csv and updates the database accordingly.
 *
 * To use this command, open a terminal window, enter into your project
 * directory and execute the following:
 *
 *     $ php bin/console app:import-product-csv
 *
 * To output detailed information, increase the command verbosity:
 *
 *     $ php bin/console app:import-product-csv -vv
 */
class ImportProductCsvCommand extends Command
{
    protected const DEFAULT_DIRECTORY = './data/ProductCsv';
    protected const DEFAULT_IN_PENNIES = false;
    protected const DEFAULT_CSV_SEPARATOR = '|';

    protected static $defaultName = 'app:import-product-csv';

    /** @var ProductRepository */
    private $productRepository;
    /** @var CsvImportTool */
    private $csvImportTool;
    /** @var ValidatorInterface */
    private $validator;
    /** @var ProductFactory */
    private $productFactory;
    /** @var SymfonyStyle */
    private $io;
    /** @var string[] */
    private $errors = [];
    /** @var int */
    private $updatedCount = 0;
    /** @var int */
    private $createdCount = 0;

    public function __construct(
        ProductRepository $productRepository,
        CsvImportTool $csvImportTool,
        ValidatorInterface $validator,
        ProductFactory $productFactory
    ) {
        $this->csvImportTool = $csvImportTool;
        $this->productRepository = $productRepository;
        $this->validator = $validator;
        $this->productFactory = $productFactory;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Import a product csv and store it in the database')
            ->addArgument('filename', InputArgument::REQUIRED, 'The filename of the csv')
            ->addArgument(
                'directory',
                InputArgument::OPTIONAL,
                'The directory of the csv',
                self::DEFAULT_DIRECTORY
            )
            ->addArgument(
                'inPennies',
                InputArgument::OPTIONAL,
                'Specify whether the monetary values within the CSV are in pennies',
                self::DEFAULT_IN_PENNIES
            )
            ->addArgument(
                'csvSeparator',
                InputArgument::OPTIONAL,
                'Specify the csv separator',
                self::DEFAULT_CSV_SEPARATOR
            );
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $importedRows = $this->loadCsvData($input);

        if (null === $importedRows){
            return Command::FAILURE;
        }

        $this->io->success('CSV successfully loaded');

        $this->importRows($importedRows);

        $this->io->success('Data imported');

        if ($output->isVerbose() && $this->getErrorCount() > 0) {
            $this->io->listing($this->getErrors());
        }

        $this->io->success(sprintf(
            'Job finished. Total CSV rows %s. Rows with errors %s. Products updated %s. Products created %s.',
            count($importedRows),
            $this->getErrorCount(),
            $this->getUpdatedCount(),
            $this->getCreatedCount()
        ));

        if (false === $this->hasImportedRows()) {
            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }

    /**
     * Import the data in 1000 row batches.
     *
     * @param array $rowsToImport
     * @return $this
     */
    protected function importRows(array $rowsToImport): self
    {
        $i = 1;
        $batchSize = 1000;

        $this->productRepository->disableLogging();

        foreach ($rowsToImport as $key => $row) {
            $product = $this->productFactory->createFromArray($row);

            $error = $this->validator->validate($product);

            if (count($error) > 0) {
                $this->addError(sprintf('Row Id: %s, sku: %s, error: %s', $key, $row['sku'], (string) $error));

                continue;
            }

            $product = $this->productRepository->save($product);

            if (($i % $batchSize) === 0) {
                $this->productRepository->flushAndClear();
            }

            $i++;

            if (null === $product->getUpdatedAt()) {
                $this->addToCreatedCount();
                continue;
            }

            $this->addToUpdatedCount();
        }

        $this->productRepository->flushAndClear();

        return $this;
    }

    protected function loadCsvData(InputInterface $input): ?array
    {
        $importedRows = null;

        $this->csvImportTool->setCsvSeparator($input->getArgument('csvSeparator'));

        try {
            $importedRows = $this->csvImportTool->importFromFileName(
                $input->getArgument('directory'),
                $input->getArgument('filename'),
                new Collection([
                    'sku' => new Required(),
                    'description' => new Required(),
                    'normal_price' => new Required(),
                    'special_price' => new Optional(),
                ])
            );
        } catch (FileNotFoundException | NoDataInFileException $e) {
            $this->io->error($e->getMessage());
        } catch (InvalidCsvFileFormatException $e) {
            $this->io->error($e->getMessage());
            $this->io->error($e->getErrors());
        }

        return $importedRows;
    }

    public function addError(string $error): self
    {
        $this->errors[] = $error;

        return $this;
    }

    public function addToUpdatedCount(): self
    {
        $this->updatedCount++;

        return $this;
    }

    public function addToCreatedCount(): self
    {
        $this->createdCount++;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    public function hasImportedRows(): bool
    {
        return $this->getCreatedCount() > 0 || $this->getUpdatedCount() > 0;
    }

    public function getErrorCount(): int
    {
        return count($this->errors);
    }

    public function getUpdatedCount(): int
    {
        return $this->updatedCount;
    }

    public function getCreatedCount(): int
    {
        return $this->createdCount;
    }
}
